﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace H.w_19._8._20
{
    public class Camp
    {
        //private readonly int id;
        //public float Latitude { get; private set; }
        //public float Longitude { get; private set; }
        //public int NumberOfPeople { get; set; }
        //public int NumberOfTents { get; private set; }
        //public int NumberOfFlashLights { get; private set; }
        public readonly int id;
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int NumberOfPeople { get; set; }
        public int NumberOfTents { get; set; }
        public int NumberOfFlashLights { get; set; }
        private static int lastCampId = 0;
        public Camp(float latitude, float longitude, int numberOfPeople, int numberOfTents, int numberOfFlashLights)
        {
            Latitude = latitude;
            Longitude = longitude;
            NumberOfPeople = numberOfPeople;
            NumberOfTents = numberOfTents;
            NumberOfFlashLights = numberOfFlashLights;
            lastCampId++;
            id = lastCampId;
        }
        public Camp()
        {

        }
        public static bool operator == (Camp c1, Camp c2)
        {
            if (c1 is null && c2 is null)
                return true;
            if (c1 is null || c2 is null)
                return false;
            return c1.id == c2.id;
        }
        public static bool operator !=(Camp c1, Camp c2)
        {
            if (c1 is null && c2 is null)
                return true;
            if (c1 is null || c2 is null)
                return false;
            return c1.id != c2.id;
        }
        public static bool operator >(Camp c1, Camp c2)
        {
            if (c1 is null || c2 is null)
                return false;
            return c1.NumberOfPeople > c2.NumberOfPeople;
        }
        public static bool operator <(Camp c1, Camp c2)
        {
            if (c1 is null || c2 is null)
                return false;
            return c1.NumberOfPeople < c2.NumberOfPeople;
        }
        public static Camp operator +(Camp c1, Camp c2)
        {
            if (c1 is null || c2 is null)
                return null;
            return new Camp(c1.Latitude + c2.Latitude, c1.Longitude + c2.Longitude,
                c1.NumberOfPeople + c2.NumberOfPeople, c1.NumberOfTents + c2.NumberOfTents,
                c1.NumberOfFlashLights + c2.NumberOfFlashLights);
        }
        public override bool Equals(object obj)
        {
            return this.id == (obj as Camp).id;
        }
        public override int GetHashCode()
        {
            return this.id;
        }
        public override string ToString()
        {
            return $"Id {this.GetHashCode()} Latitude {Latitude} Longitude {Longitude}" +
                $" Number of people {NumberOfPeople} Number of tents {NumberOfTents}" +
                $" Number of flash lights {NumberOfFlashLights}";
        }
        public static void SerializeACamp(Camp camp, string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Camp));
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(file, camp);
            }
        }
        public static Camp DesiralizeACamp (string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Camp));
            Camp camp = null;
            using (Stream file=new FileStream(fileName,FileMode.Open))
            {
                camp = serializer.Deserialize(file) as Camp;
            }
            return camp;
        }
    }
}
