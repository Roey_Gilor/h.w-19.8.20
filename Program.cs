﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.w_19._8._20
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp camp1 = new Camp(120.5f, 21.5f, 5, 3, 6);
            //Camp camp2 = new Camp(620.8f, 2.5f, 15, 13, 10);
            //if (camp1 > camp2)
            //    Console.WriteLine("Camp 1 have more people than camp 2");
            //else
            //    Console.WriteLine("Camp 2 have more people than camp 1");
            //Console.WriteLine((camp1+camp2).ToString());
            Camp.SerializeACamp(camp1, @"..\..\xmlfile.xml");
            Camp camp3 = Camp.DesiralizeACamp(@"..\..\xmlfile.xml");
            Camp camp4 = Camp.DesiralizeACamp(@"..\..\xmlfile.xml");
            Console.WriteLine(camp3 == camp4);
            Console.WriteLine(camp3.Equals(camp4));
        }
    }
}
